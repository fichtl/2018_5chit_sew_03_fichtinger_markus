﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Contexts;
using System.Data.Entity;

namespace InheritanceStrategies
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new BillingDetailContext())
            {
                BankAccount bankAcc = new BankAccount()
                {
                    Owner = "Markus",
                    Number = "987654321"
                };
                context.BillingDetails.Add(bankAcc);
                context.SaveChanges();
            }
        }
    }

    public abstract class BillingDetail
    {
        public int BillingDetailId { get; set; }
        public string Owner { get; set; }
        public string Number { get; set; }
    }

    public class BankAccount : BillingDetail
    {
        public string BankName { get; set; }
        public string Swift { get; set; }
    }

    public class CreditCard : BillingDetail
    {
        public int CardType { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class BillingDetailContext : DbContext
    {
        public DbSet<BillingDetail> BillingDetails { get; set; }

        public BillingDetailContext() : base("Single_Table")
        {

        }
    }

}
