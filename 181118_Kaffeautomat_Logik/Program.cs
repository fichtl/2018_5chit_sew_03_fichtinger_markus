﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;

            IAmount i = new Amount500Ml();

            ACoffee ac = new GreenTea();
            Milk m = new Milk(ac);
            Honey h = new Honey(m);
            Sugar s = new Sugar(h);

            Console.WriteLine("Ihr Kaffee beinhaltet: ");
            Console.WriteLine(s.GetName());

            Console.WriteLine();

            Console.WriteLine("Ihr Kaffee kostet: ");
            Console.WriteLine(s.GetPrice(i) + " Euro");

            Console.ReadKey();
        }
    }
}
