﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    abstract class ACoffee
    {
        public abstract string GetName();
        public abstract double GetPrice(IAmount a);
    }
}
