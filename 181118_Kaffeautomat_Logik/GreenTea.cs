﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    class GreenTea : ACoffee
    {
        public override string GetName()
        {
            return "Green Tea";
        }

        public override double GetPrice(IAmount a)
        {
            if (a.Amount() == 200)
                return 200.00;
            else if (a.Amount() == 500)
                return 400.00;
            else
                return 0;
        }
    }
}
