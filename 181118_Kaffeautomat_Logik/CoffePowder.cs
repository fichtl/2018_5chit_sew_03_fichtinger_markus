﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    class CoffePowder : ACoffee
    {
        public override string GetName()
        {
            return "CoffePowder";
        }

        public override double GetPrice(IAmount a)
        {
            if (a.Amount() == 200)
                return 1.00;
            else if (a.Amount() == 500)
                return 2.00;
            else
                return 0;
        }
    }
}
