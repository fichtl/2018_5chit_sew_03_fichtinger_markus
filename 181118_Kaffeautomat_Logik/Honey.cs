﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    class Honey : AIngredients
    {
        public override string GetName()
        {
            return base.GetName() + ", Honey";
        }

        public Honey(ACoffee coffee) : base(coffee)
        {

        }

        public override double GetPrice(IAmount a)
        {
            if (a.Amount() == 200)
                return base.GetPrice(a) + 0.50;

            else if (a.Amount() == 500)
                return base.GetPrice(a) + 1.00;

            else
                return 0;
        }
    }
}
