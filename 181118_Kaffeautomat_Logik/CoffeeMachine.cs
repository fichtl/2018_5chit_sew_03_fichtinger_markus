﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    //Singleton
    public class CoffeeMachine
    {
        private CoffeeMachine cM = new CoffeeMachine();

        private CoffeeMachine()
        {

        }

        public CoffeeMachine GetInstance()
        {
            return cM;
        }

    }
}
