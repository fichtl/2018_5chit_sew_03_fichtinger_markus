﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    class Amount200Ml : IAmount
    {
        public int Amount()
        {
            return 200;
        }
    }
}
