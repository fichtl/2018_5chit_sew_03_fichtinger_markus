﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaffeautomat
{
    //Decorator
    abstract class AIngredients : ACoffee
    {
        protected ACoffee c;

        public AIngredients(ACoffee c)
        {
            this.c = c;
        }

        public override string GetName()
        {
            return c.GetName();
        }

        public override double GetPrice(IAmount a)
        {
            return c.GetPrice(a);
        }

    }
}
