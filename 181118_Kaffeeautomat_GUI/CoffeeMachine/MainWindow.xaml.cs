﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CoffeeMachine
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ACoffee greenTea = new GreenTea();
        ACoffee Cocoa = new Cocoa();
        ACoffee coffePowder = new CoffeePowder();

        ACoffee milk = new Milk();
        ACoffee sugar = new Sugar();
        ACoffee honey = new Honey();

        IAmount _200 = new Amount200Ml();
        IAmount _500 = new Amount500Ml();

        string name;
        double price;
        int paid = 0;
        public MainWindow()
        {
            InitializeComponent();
            
            img_machine.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory+ "automat.png"));

            
            lb_CoffeeType.Items.Add(greenTea);
            lb_CoffeeType.Items.Add(Cocoa);
            lb_CoffeeType.Items.Add(coffePowder);

            lb_Ingredients.Items.Add(milk);
            lb_Ingredients.Items.Add(sugar);
            lb_Ingredients.Items.Add(honey);

            lb_Amount.Items.Add("200ml");
            lb_Amount.Items.Add("500ml");
            
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            List<ACoffee> listcoffee = new List<ACoffee>();
            listcoffee.Add((ACoffee)lb_Ingredients.SelectedItem);

            if ((ACoffee)lb_Ingredients.SelectedItem == milk)
            {
                name += milk.GetName();
                if (lb_Amount.SelectedItem.ToString() == "200ml")
                    price += milk.GetPrice(_200);

                else if (lb_Amount.SelectedItem.ToString() == "500ml")
                    price += milk.GetPrice(_500);
            }

            else if ((ACoffee)lb_Ingredients.SelectedItem == sugar)
            {
                name += sugar.GetName();
                if (lb_Amount.SelectedItem.ToString() == "200ml")
                    price += sugar.GetPrice(_200);

                else if (lb_Amount.SelectedItem.ToString() == "500ml")
                    price += sugar.GetPrice(_500);
            }

            else if ((ACoffee)lb_Ingredients.SelectedItem == honey)
            {
                name += honey.GetName();
                if (lb_Amount.SelectedItem.ToString() == "200ml")
                    price += honey.GetPrice(_200);

                else if (lb_Amount.SelectedItem.ToString() == "500ml")
                    price += honey.GetPrice(_500);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ((ACoffee)lb_CoffeeType.SelectedItem == Cocoa)
            {
                name += Cocoa.GetName();

                if (lb_Amount.SelectedItem.ToString() == "200ml")
                    price += Cocoa.GetPrice(_200);

                else if (lb_Amount.SelectedItem.ToString() == "500ml")
                    price += Cocoa.GetPrice(_500);
            }

            else if ((ACoffee)lb_CoffeeType.SelectedItem == coffePowder)
            {
                name += coffePowder.GetName();

                if (lb_Amount.SelectedItem.ToString() == "200ml")
                    price += coffePowder.GetPrice(_200);

                else if (lb_Amount.SelectedItem.ToString() == "500ml")
                    price += coffePowder.GetPrice(_500);
            }

            else if ((ACoffee)lb_CoffeeType.SelectedItem == greenTea)
            {
                name += greenTea.GetName();

                if (lb_Amount.SelectedItem.ToString() == "200ml")
                    price += greenTea.GetPrice(_200);

                else if (lb_Amount.SelectedItem.ToString() == "500ml")
                    price += greenTea.GetPrice(_500);
            }

            MessageBox.Show("Artikel: " + name + "\n" + "Preis:" + price.ToString() + "€");
        }

        private void btn_Coin_Click(object sender, RoutedEventArgs e)
        {
            paid = 1;
        }

        private void btn_Coffee_Click(object sender, RoutedEventArgs e)
        {
            if (paid == 0)
            {
                MessageBox.Show("Zerst zahlen, dann gibts an Kaffee ;)");
            }

            else if(paid == 1)
            {
                MessageBox.Show("Daunksche");
                paid = 0;
                lb_CoffeeType.SelectedItem = null;
                lb_Ingredients.SelectedItem = null;
                lb_Amount.SelectedItem = null;
                price = 0;
                name = "";
            }
        }
    }
}
