﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeMachine
{
    abstract class ACoffee
    {
        public abstract string GetName();
        public abstract double GetPrice(IAmount amount);
    }

    class Cocoa : ACoffee
    {
        public override string GetName()
        {
            return "Cocoa ";
        }

        public override double GetPrice(IAmount amount)
        {
            if(amount.Amount() == 200)
            return 1.0;

            else if (amount.Amount() == 500)
                return 2.0;

            else
                return 0;
        }
    }

    class CoffeePowder : ACoffee
    {
        public override string GetName()
        {
            return "Coffee Powder ";
        }

        public override double GetPrice(IAmount amount)
        {
            if (amount.Amount() == 200)
                return 1.0;

            else if (amount.Amount() == 500)
                return 2.0;

            else
                return 0;
        }
    }

    class GreenTea : ACoffee
    {
        public override string GetName()
        {
            return "Green Tea ";
        }

        public override double GetPrice(IAmount amount)
        {
            if (amount.Amount() == 200)
                return 1.0;

            else if (amount.Amount() == 500)
                return 2.0;

            else
                return 0;
        }
    }
}
