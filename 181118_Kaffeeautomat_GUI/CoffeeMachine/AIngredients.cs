﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeMachine
{
    abstract class AIngredients : ACoffee
    {
        protected ACoffee coffee = null;

        public void SetCoffee(ACoffee coffee)
        {
            this.coffee = coffee;
        }

        public override string GetName()
        {
            if (coffee != null)
                return coffee.GetName();

            else
                return "";
        }

        public override double GetPrice(IAmount amount)
        {
            if (coffee != null)
                return coffee.GetPrice(amount);

            else
                return 0;
        }

        public AIngredients(ACoffee coffee)
        {
            this.coffee = coffee;
        }

        public AIngredients() { }
    }

    class Milk : AIngredients
    {
        public Milk(ACoffee coffee) : base(coffee) { }
        public Milk() { }
        public override string GetName()
        {
            return base.GetName() + "Milk "; 
        }

        public override double GetPrice(IAmount amount)
        {
            if (amount.Amount() == 200)
                return base.GetPrice(amount) + 0.50;

            else if (amount.Amount() == 500)
                return base.GetPrice(amount) + 1.00;

            else
                return base.GetPrice(amount) + 0;
        }
    }

    class Sugar : AIngredients
    {
        public Sugar(ACoffee coffee) : base(coffee) { }
        public Sugar() { }
        public override string GetName()
        {
            return base.GetName() + "Sugar ";
        }

        public override double GetPrice(IAmount amount)
        {
            if (amount.Amount() == 200)
                return base.GetPrice(amount) + 0.50;

            else if (amount.Amount() == 500)
                return base.GetPrice(amount) + 1.00;

            else
                return base.GetPrice(amount) + 0;
        }
    }

    class Honey : AIngredients
    {
        public Honey(ACoffee coffee) : base(coffee) { }
        public Honey() { }
        public override string GetName()
        {
            return base.GetName() + "Honey ";
        }

        public override double GetPrice(IAmount amount)
        {
            if (amount.Amount() == 200)
                return base.GetPrice(amount) + 0.50;

            else if (amount.Amount() == 500)
                return base.GetPrice(amount) + 1.00;

            else
                return base.GetPrice(amount) + 0;
        }
    }
}
