﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeMachine
{
    class Machine
    {
        private Machine machine = new Machine();
        private Machine() { }

        public Machine GetMachine()
        {
            return machine;
        }
    }
}
