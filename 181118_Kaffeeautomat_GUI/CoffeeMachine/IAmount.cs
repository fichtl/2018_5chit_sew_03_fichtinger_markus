﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeMachine
{
    interface IAmount
    {
        int Amount();
    }

    class Amount200Ml : IAmount
    {
        public int Amount()
        {
            return 200;
        }
    }

    class Amount500Ml : IAmount
    {
        public int Amount()
        {
            return 500;
        }
    }
}
