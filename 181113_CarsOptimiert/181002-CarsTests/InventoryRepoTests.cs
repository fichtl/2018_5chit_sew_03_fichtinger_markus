﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using _181002_Cars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars.Tests
{
    [TestClass()]
    public class InventoryRepoTests
    {
        InventoryRepo i = new InventoryRepo();

        [TestMethod()]
        public void GetBlackCars()
        {
            string[] arr = { "VW", "Saab", "BMW", "Pinto" };
            List<Car> x = i.GetBlackCars();

            for (int i = 0; i < arr.Length; i++)
            {
                Assert.AreEqual(arr[i], x[i].Make);
            }
        }

        [TestMethod()]
        public void GetCarMakesTest()
        {
            object[] arr = { "VW", "Ford", "Saab", "Yugo", "BMW", "BMW", "BMW", "Pinto", "Yugo", "VW" };
            object[] x = i.GetCarMakes();

            for (int a = 0; a < arr.Length; a++)
            {
                Assert.AreEqual(arr[a], x[a]);
            }
        }

    }
}