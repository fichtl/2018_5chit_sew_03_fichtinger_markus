﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
    class InventoryViewModel : AViewModel<Car>
    {
        public InventoryViewModel(Car c)
        {
            this.Entity = c;
        }

        public int CarId
        {
            get
            {
                return Entity.CarId;
            }
            set
            {
                Entity.CarId = value;
                CallEvent("CarId");
            }
        }

        public string Make
        {
            get
            {
                return Entity.Make;
            }
            set
            {
                Entity.Make = value;
                CallEvent("Make");
            }
        }

        public string Color
        {
            get
            {
                return Entity.Color;
            }
            set
            {
                Entity.Color = value;
                CallEvent("Color");
            }
        }

        public string CarNickName
        {
            get
            {
                return Entity.CarNickName;
            }
            set
            {
                Entity.CarNickName = value;
                CallEvent("CarNickName");
            }
        }

    }
}
