﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _181002_Cars
{
    /// <summary>
    /// Interaktionslogik für AddOrder.xaml
    /// </summary>
    public partial class AddOrder : Window
    {
        public event Action Change;

        public AddOrder()
        {
            InitializeComponent();

            Context context = new Context();

            var getCustomerId = from id in context.Customers select id.CustId;
            var getCarId = from id in context.Inventory select id.CarId;

            foreach (var item in getCustomerId)
                lb_Customer.Items.Add(item);

            foreach (var item in getCarId)
                lb_Car.Items.Add(item);

        }

        private void btn_Save_Added_Order_Click(object sender, RoutedEventArgs e)
        {
            using (var repo = new OrderRepo())
            {

                if (lb_Car != null && lb_Car.SelectedItem != null && lb_Customer != null && lb_Customer.SelectedItem != null)
                {
                    Order o = new Order() { CustId = (int)lb_Customer.SelectedItem, CarId = (int)lb_Car.SelectedItem };
                    repo.Add(o);
                    Change();
                    MessageBox.Show("added");
                    this.Close();
                }
            }
        }
    }
}
