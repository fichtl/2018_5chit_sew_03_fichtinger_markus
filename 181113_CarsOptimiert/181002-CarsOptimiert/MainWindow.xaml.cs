﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;
using System.Data.Entity;
using System.ComponentModel;

namespace _181002_Cars
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private CustomerRepo customerRepo = new CustomerRepo();
        private OrderRepo orderRepo = new OrderRepo();
        private InventoryRepo inventoryRepo = new InventoryRepo();
        private CreditRiskRepo creditRepo = new CreditRiskRepo();

        public MainWindow()
		{
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            dg_customers.ItemsSource = customerRepo.GetAll().ToViewModel(customer => new CustomerViewModel(customer));
            dg_orders.ItemsSource = orderRepo.GetAll().ToViewModel(order => new OrderViewModel(order));
            dg_inventory.ItemsSource = inventoryRepo.GetAll().ToViewModel(inventory => new InventoryViewModel(inventory));
            dg_creditrisks.ItemsSource = creditRepo.GetAll().ToViewModel(creditrisk => new CreditRiskViewModel(creditrisk));

            /*dg_creditrisks.ItemsSource = customerRepo.getCustomerStartWithW();*/
        }



        //Customer
		private void Add_Customer_Click(object sender, RoutedEventArgs e)
        {
            AddCustomer ac = new AddCustomer();
            ac.Change += UpdateGrid;
			ac.Show();
        }

		private void btn_Delete_Customer_Click(object sender, RoutedEventArgs e)
		{
			if (dg_customers.SelectedItem != null)
			{
				Customer c = ((CustomerViewModel)dg_customers.SelectedItem).GetEntity();
                customerRepo.Delete(c);
                UpdateGrid();
				MessageBox.Show("deleted");
				
			}
		}

        private void dg_customers_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            customerRepo.SaveChanges();
            UpdateGrid();
        }



        //Inventory
        private void Add_Inventory_Click(object sender, RoutedEventArgs e)
        {
            AddInventory ai = new AddInventory();
            ai.Change += UpdateGrid;
            ai.Show();
        }

        private void Delete_Inventory_Click(object sender, RoutedEventArgs e)
        {
            if (dg_inventory.SelectedItem != null)
            {
                Car i = ((InventoryViewModel)dg_inventory.SelectedItem).GetEntity();
                inventoryRepo.Delete(i);
                UpdateGrid();
                MessageBox.Show("deleted");

            }
        }

        private void dg_inventory_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            inventoryRepo.SaveChanges();
            UpdateGrid();
        }



        //Order
        private void Add_Order_Click(object sender, RoutedEventArgs e)
        {
            AddOrder ao = new AddOrder();
            ao.Change += UpdateGrid;
            ao.Show();
        }

        private void Delete_Order_Click(object sender, RoutedEventArgs e)
        {
            if (dg_orders.SelectedItem != null)
            {
                Order o = ((OrderViewModel)dg_orders.SelectedItem).GetEntity();
                orderRepo.Delete(o);
                UpdateGrid();
                MessageBox.Show("deleted");

            }
        }

        private void dg_orders_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            orderRepo.SaveChanges();
            UpdateGrid();
        }



        //CreditRisk
        private void Add_CreditRisk_Click(object sender, RoutedEventArgs e)
        {
            AddCreditRisk aCr = new AddCreditRisk();
            aCr.Change += UpdateGrid;
            aCr.Show();
        }

        private void Delete_CreditRisk_Click(object sender, RoutedEventArgs e)
        {
            if (dg_creditrisks.SelectedItem != null)
            {
                CreditRisk cr = ((CreditRiskViewModel)dg_creditrisks.SelectedItem).GetEntity();
                creditRepo.Delete(cr);
                UpdateGrid();
                MessageBox.Show("deleted");

            }
        }

        private void dg_creditrisks_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            creditRepo.SaveChanges();
            UpdateGrid();
        }








        private void dg_customers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dg_inventory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dg_orders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dg_creditrisks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btn_ShowCustomerQueries_Click(object sender, RoutedEventArgs e)
        {
            CustomerQueries cq = new CustomerQueries();
            cq.Show();
        }
    }
}
