﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
	public partial class Order
	{
		[Key, Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int OrderId { get; set; }
		[Required]
		public int CustId { get; set; }
		[Required]
		public int CarId { get; set; }
		[Timestamp]
		public byte[] Timestamp { get; set; }
		[ForeignKey("CustId")]
		public virtual Customer Customer { get; set; }
		[ForeignKey("CarId")]
		public virtual Car Car { get; set; }
    }
}
