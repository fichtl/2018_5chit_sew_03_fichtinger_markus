﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
	class OrderRepo : BaseRepo<Order>//, IRepo<Order>
	{
        Context c = new Context();

        public OrderRepo()
		{
			Table = Context.Orders;
		}

        public IQueryable GetOrdersByCustomer(Customer cust)
        {
            return c.Orders.Where(x => x.CustId == cust.CustId).Select(x => x.CustId);
        }

        public int GetCountOrders()
        {
            return c.Orders.Select(x => x.OrderId).Count();
        }

        public IQueryable GetOrderDetailsById(int id)
        {
            return from x in c.Orders where x.OrderId == id select new { x.CustId, x.CarId };
        }

        //public int Delete(int id, byte[] timestamp)
        //{
        //	Context.Entry(new Order()
        //	{
        //		OrderId = id,
        //		Timestamp=timestamp
        //	}).State = System.Data.Entity.EntityState.Deleted;
        //	return SaveChanges();
        //}
    }
}
