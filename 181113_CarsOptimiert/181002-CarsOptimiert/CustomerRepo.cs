﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
	class CustomerRepo : BaseRepo<Customer>//, IRepo<Customer>
	{
        Context c = new Context();

        public CustomerRepo()
		{
			Table = Context.Customers;
		}

        public object[] GetCustomersStartsWithW()
        {
            return (from x in c.Customers where x.LastName.ToUpper().StartsWith("W") select new { CustId = x.CustId, FirstName = x.FirstName, LastName = x.LastName }).ToArray();
        }

        public object[] GetFirstThreeCustomers()
        {
            return (c.Customers.Take(3).Select(x => new { FirstName = x.FirstName, LastName = x.LastName })).ToArray();
        }

        public int GetCount()
        {
            return c.Customers.Select(x => x.CustId).Count();
        }

        //public int Delete(int id, byte[] timestamp)
        //{
        //	Context.Entry(new Customer()
        //	{
        //		CustId = id,
        //		Timestamp = timestamp
        //	}).State = System.Data.Entity.EntityState.Deleted;
        //	return SaveChanges();
        //}

    }
}
