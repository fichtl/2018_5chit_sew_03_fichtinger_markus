﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
	public class Context:DbContext
	{
		public Context() : base("CarsDB") {
			Database.SetInitializer(new Initializer());
		}

		public DbSet<Customer> Customers { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Car> Inventory { get; set; }
		public DbSet<CreditRisk> CreditRisks { get; set; }
	}
}
