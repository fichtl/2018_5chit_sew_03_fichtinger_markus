﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
    class CreditRiskRepo : BaseRepo<CreditRisk>//, IRepo<CreditRisk>
	{
        Context c = new Context();

        public CreditRiskRepo()
        {
            Table = Context.CreditRisks;
        }

        public IQueryable GetFirstName()
        {
            return from x in c.CreditRisks select x.FirstName;
        }

        public IQueryable GetLastName()
        {
            return from x in c.CreditRisks select x.LastName;
        }

        public int GetCount()
        {
            return (from x in c.CreditRisks select x.CustId).Count();
        }

        //public int Delete(int id, byte[] timestamp)
        //{
        //	Context.Entry(new CreditRisk()
        //	{
        //		CustId = id,
        //		Timestamp = timestamp
        //	}).State = System.Data.Entity.EntityState.Deleted;
        //	return SaveChanges();
        //}

    }
}
