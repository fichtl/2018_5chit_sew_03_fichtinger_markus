﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _181002_Cars
{
    /// <summary>
    /// Interaktionslogik für CustomerQueries.xaml
    /// </summary>
    public partial class CustomerQueries : Window
    {
        private CustomerRepo customerRepo = new CustomerRepo();

        public CustomerQueries()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            lblCount.Content = customerRepo.GetCount().ToString();

            dg_getCustomersStartsWithW.ItemsSource = customerRepo.GetCustomersStartsWithW();

            object[] arr = customerRepo.GetFirstThreeCustomers();
            dg_FirstThree.ItemsSource = customerRepo.GetFirstThreeCustomers();
        }

    }
}
