﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
    static class Extensions
    {
        public static string ExtenCustomer(this Customer c)
        {
            return c.CustId + " " + c.FirstName + " " + c.LastName;
        }

        public static string ExtenInventory(this Car c)
        {
            return c.Make + " " + c.CarNickName;
        }

        public static string ExtenOrders(this Order o)
        {
            return o.OrderId + " " + o.CarId;
        }

        public static IEnumerable<TViewModel> ToViewModel<TEntity, TViewModel>(this IEnumerable<TEntity> entities, Func<TEntity, TViewModel> instantiate)
            where TViewModel : AViewModel<TEntity>
        => (from e in entities select instantiate(e)).ToList();
        /*{
            List<TViewModel> viewModels = new List<TViewModel>();
            foreach (TEntity item in entities)
            {
                viewModels.Add(instantiate(item));
            }
            return viewModels;
        }*/

    }
}
