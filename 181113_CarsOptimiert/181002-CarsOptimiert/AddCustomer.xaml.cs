﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _181002_Cars
{
	/// <summary>
	/// Interaktionslogik für AddCustomer.xaml
	/// </summary>
	public partial class AddCustomer : Window
	{
        public event Action Change;

		public AddCustomer()
		{
			InitializeComponent();
		}

		private void btn_Save_Added_Customer_Click(object sender, RoutedEventArgs e)
		{
			using (var repo = new CustomerRepo())
			{

				if (tx_FirstName != null && tx_FirstName.Text != "" && tx_LastName != null && tx_LastName.Text != "")
				{
					Customer c = new Customer() { FirstName = tx_FirstName.Text, LastName = tx_LastName.Text };
					repo.Add(c);
                    Change();
					MessageBox.Show("added");
					this.Close();
				}
			}
		}
	}
}
