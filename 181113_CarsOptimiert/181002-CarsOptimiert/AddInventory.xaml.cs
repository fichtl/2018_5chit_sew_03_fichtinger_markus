﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _181002_Cars
{
    /// <summary>
    /// Interaktionslogik für AddInventory.xaml
    /// </summary>
    public partial class AddInventory : Window
    {
        public event Action Change;

        public AddInventory()
        {
            InitializeComponent();
        }

        private void btn_Save_Added_Car_Click(object sender, RoutedEventArgs e)
        {
            using (var repo = new InventoryRepo())
            {

                if (tx_CarNickName != null && tx_CarNickName.Text != "" && tx_Color != null && tx_Color.Text != "" && tx_Make != null && tx_Make.Text != "")
                {
                    Car c = new Car() { Make = tx_Make.Text, Color = tx_Color.Text, CarNickName = tx_CarNickName.Text };
                    repo.Add(c);
                    Change();
                    MessageBox.Show("added");
                    this.Close();
                }
            }
        }
    }
}
