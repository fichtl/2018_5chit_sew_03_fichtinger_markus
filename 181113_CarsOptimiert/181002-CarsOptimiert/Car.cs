﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
	[Table("Inventory")]
	public partial class Car
	{
		[Key]
		public int CarId { get; set; }
		[StringLength(50)]
		public string Make { get; set; }
		[StringLength(50)]
		public string Color { get; set; }
		[StringLength(50)]
		public string CarNickName { get; set; }
		[Timestamp]
		public byte[] Timestamp { get; set; }

		public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();

		private static void AddNewRecord(Car car)
		{
			using (var repo = new InventoryRepo())
			{
				repo.Add(car);
			}
		}
		private static void AddNewRecords(IList<Car> cars)
		{
			using (var repo = new InventoryRepo())
			{
				repo.AddRange(cars);
			}
		}
		private static void ShowAllOrders()
		{
			using (var repo = new OrderRepo())
			{
				Console.WriteLine("************** Pending Orders **************");
				foreach (var item in repo.GetAll())
				{
					Console.WriteLine($"->{item.Customer.FullName} is waiting on {item.Car.CarNickName}");
				}
			}
		}
		private static void ShowAllOrdersEagerlyFetched()
		{
			using (var context = new Context())
			{
				Console.WriteLine("************** Pending Orders **************");
				var orders = context.Orders
					.Include("Customer").Include("Car").ToList();
				foreach (var item1 in orders)
				{
					Console.WriteLine($"->{item1.Customer.FullName} is waiting on {item1.Car.CarNickName}");
				}
			}
		}
		private static void UpdateRecord(int carid)
		{
			using (var repo = new InventoryRepo())
			{
				var carToUpdate = repo.GetOne(carid);
				if (carToUpdate != null)
				{
					Console.WriteLine("Before change: " + repo.Context.Entry(carToUpdate).State);
					carToUpdate.Color = "Blue";
					Console.WriteLine("After change: " + repo.Context.Entry(carToUpdate).State);
					repo.Save(carToUpdate);
					Console.WriteLine("After save: " + repo.Context.Entry(carToUpdate).State);

				}
			}
		}
	}

		public partial class Car
		{
			public override string ToString()
			{
				// Since the PetName column could be empty, supply
				// the default name of **No Name**.
				return $"{this.CarNickName ?? "**No Name**"} is a" +
					$" {this.Color} {this.Make} with ID {this.CarId}.";
			}

			[NotMapped]
			public string MakeColor => $"{Make} + ({Color})";
		}
	
}
