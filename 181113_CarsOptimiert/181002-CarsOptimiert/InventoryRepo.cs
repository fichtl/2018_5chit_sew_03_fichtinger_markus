﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _181002_Cars
{
	public class InventoryRepo : BaseRepo<Car>//, IRepo<Car>
	{
        Context c = new Context();

		public InventoryRepo()
		{
			Table = Context.Inventory;
		}

        public List<Car> GetBlackCars()
        {
            return (c.Inventory.Where(x => x.Color.ToUpper() == "BLACK").Select(x => x)).ToList();
        }

        public object[] GetCarMakes()
        {
            return (c.Inventory.Select(x => x.Make)).ToArray();
        }

        public object[] GetCarByMake(string name)
        {
            return (c.Inventory.Where(x => x.Make.ToString() == name).Select(x => x)).ToArray();
        }

        //public int Delete(int id, byte[] timestamp)
        //{
        //	Context.Entry(new Car()
        //	{
        //		CarId = id,
        //		Timestamp = timestamp
        //	}).State = System.Data.Entity.EntityState.Deleted;
        //	return SaveChanges();
        //}
    }
}
