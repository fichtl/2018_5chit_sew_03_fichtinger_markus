﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
    class CreditRiskViewModel : AViewModel<CreditRisk>
    {
        public CreditRiskViewModel(CreditRisk cr)
        {
            this.Entity = cr;
        }

        public int CustId
        {
            get
            {
                return Entity.CustId;
            }
            set
            {
                Entity.CustId = value;
                CallEvent("CustId");
            }
        }

        public string FirstName
        {
            get
            {
                return Entity.FirstName;
            }
            set
            {
                Entity.FirstName = value;
                CallEvent("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return Entity.LastName;
            }
            set
            {
                Entity.LastName = value;
                CallEvent("LastName");
            }
        }

    }
}
