﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
    class OrderViewModel : AViewModel<Order>
    {
        public OrderViewModel(Order o)
        {
            this.Entity = o;
        }

        public int OrderId
        {
            get
            {
                return Entity.OrderId;
            }
            set
            {
                Entity.OrderId = value;
                CallEvent("OrderId");
            }
        }

        public int CustId
        {
            get
            {
                return Entity.CustId;
            }
            set
            {
                Entity.CustId = value;
                CallEvent("CustId");
            }
        }

        public int CarId
        {
            get
            {
                return Entity.CarId;
            }
            set
            {
                Entity.CarId = value;
                CallEvent("CarId");
            }
        }

    }
}
