﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _181002_Cars
{
    /// <summary>
    /// Interaktionslogik für AddCreditRisk.xaml
    /// </summary>
    public partial class AddCreditRisk : Window
    {
        public event Action Change;

        public AddCreditRisk()
        {
            InitializeComponent();

            Context context = new Context();
            var getCustomerId = from id in context.Customers select new { id.CustId };

            foreach (var item in getCustomerId)
                lb_Customer.Items.Add(item.CustId);
        }

        private void btn_Save_Added_CreditRisk_Click(object sender, RoutedEventArgs e)
        {
            using (var repo = new CreditRiskRepo())
            {
                if (lb_Customer != null && lb_Customer.SelectedItem != null)
                {
                    Context context = new Context();
                    var getFirstLast = from id in context.Customers where (int)lb_Customer.SelectedItem == id.CustId select new {id.CustId, id.FirstName, id.LastName };

                    foreach (var item in getFirstLast)
                    {
                        CreditRisk cR = new CreditRisk() { CustId = item.CustId, FirstName = item.FirstName, LastName = item.LastName};
                        repo.Add(cR);
                    }

                    Change();
                    MessageBox.Show("added");
                    this.Close();
                }
            }
        }

    }
}
