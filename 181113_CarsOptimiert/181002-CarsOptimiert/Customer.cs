﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181002_Cars
{
	public partial class Customer
	{
		[Key]
		public int CustId { get; set; }
		[StringLength(50)]
		public string FirstName { get; set; }
		[StringLength(50)]
		public string LastName { get; set; }
		[NotMapped]
		public string FullName => FirstName + " " + LastName;
		[Timestamp]
		public byte[] Timestamp { get; set; }

		public virtual List<Order> Orders { get; set; } = new List<Order>();


        public override string ToString() => FullName;

    }
}
