﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Optimieren
namespace _181002_Cars
{
	public abstract class BaseRepo<T> : IDisposable where T : class, new()
	{

		public Context Context { get; } = new Context();
		protected DbSet<T> Table;

		public T GetOne(int? id) => Table.Find(id);
		public List<T> GetAll() => Table.ToList();
		public int Add(T entity)
		{
			Table.Add(entity);
			return SaveChanges();
		}
		public int AddRange(IList<T> entities)
		{
			Table.AddRange(entities);
			return SaveChanges();
		}
		public int AddRange(IEnumerable<T> entities)
		{
			Table.AddRange(entities);
			return SaveChanges();
		}
		public int Save(T entity)
		{
			Context.Entry(entity).State = EntityState.Modified;
			return Context.SaveChanges();
		}
		public int Delete(T entity)
		{
			Table.Remove(entity);
			return SaveChanges();
		}

		public List<T> ExecuteQuery(string sql) => Table.SqlQuery(sql).ToList();
		public List<T> ExecuteQuery(string sql, object[] sqlParametersObjects)
		{
			throw new NotImplementedException();
		}
		internal int SaveChanges()
		{
			return Context.SaveChanges();
		}


		bool _disposed = false;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (_disposed)
				return;

			if (disposing)
			{
				Context.Dispose();
			}
		}
	}
}
