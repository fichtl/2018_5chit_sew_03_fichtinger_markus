﻿#pragma checksum "..\..\AddInventory.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7BE59385EC956F8EAA7F6F78B81878F1FFC42E7E"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using _181002_Cars;


namespace _181002_Cars {
    
    
    /// <summary>
    /// AddInventory
    /// </summary>
    public partial class AddInventory : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_Add;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tb_Make;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tx_Make;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tb_Color;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tx_Color;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tb_CarNickName;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tx_CarNickName;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\AddInventory.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_Save_Added_Car;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/181002-Cars;component/addinventory.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AddInventory.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lbl_Add = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.tb_Make = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.tx_Make = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.tb_Color = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.tx_Color = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.tb_CarNickName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.tx_CarNickName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.btn_Save_Added_Car = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\AddInventory.xaml"
            this.btn_Save_Added_Car.Click += new System.Windows.RoutedEventHandler(this.btn_Save_Added_Car_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

