﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneToOne
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new StudentContext())
            {
                Student s = new Student()
                {
                    StudentName = "Kletzl",
                    Address = new StudentAddress()
                    {
                        Address1 = "Dahoam",
                        City = "Dahoam City"
                    }
                };

                context.students.Add(s);
                context.SaveChanges();
            }
        }
    }


    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }

        public virtual StudentAddress Address { get; set; }
    }

    public class StudentAddress
    {
        [ForeignKey("Student")]
        public int StudentAddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int Zipcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public virtual Student Student { get; set; }
    }

    public class StudentContext : DbContext
    {
        public DbSet<Student> students { get; set; }
        public DbSet<StudentAddress> studentsAddress { get; set; }

        public StudentContext() : base("OneToOne")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configure Student & StudentAddress entity
            modelBuilder.Entity<Student>()
                        .HasOptional(s => s.Address) // Mark Address property optional in Student entity
                        .WithRequired(ad => ad.Student); // mark Student property as required in StudentAddress entity. Cannot save StudentAddress without Student
        }
    }

    

}
