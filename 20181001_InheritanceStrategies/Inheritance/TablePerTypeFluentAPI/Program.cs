﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace TablePerTypeDataAnnotations
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new BillingContext())
            {
                BankAccount bankAcc = new BankAccount()
                {
                    BankName = "Waldviertler Sparkasse",
                    Swift = "12345"
                };
                context.BillingDetails.Add(bankAcc);
                context.SaveChanges();
            }
        }
    }
    public abstract class BillingDetail
    {
        public int BillingDetailId { get; set; }
        public string Owner { get; set; }
        public string Number { get; set; }
    }


    public class BankAccount : BillingDetail
    {
        public string BankName { get; set; }
        public string Swift { get; set; }
    }


    public class CreditCard : BillingDetail
    {
        public int CardType { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class BillingContext : DbContext
    {
        public DbSet<BillingDetail> BillingDetails { get; set; }

        public BillingContext() : base("Joined_Table_FluentAPI")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankAccount>().ToTable("BankAccounts");
            modelBuilder.Entity<CreditCard>().ToTable("CreditCards");
        }

    }
}
